import { countSumOfTwoRepresentations } from './count-sum-of-two-representations';

describe('Tests', () => {
  test('test1', () => {
    expect(countSumOfTwoRepresentations(6,2,4)).toEqual(2);
  });
  test('test2', () => {
    expect(countSumOfTwoRepresentations(6,3,3)).toEqual(1);
  });
  test('test3', () => {
    expect(countSumOfTwoRepresentations(10,9,11)).toEqual(0);
  });
  test('test4', () => {
    expect(countSumOfTwoRepresentations(24,8,16)).toEqual(5);
  });
  test('test5', () => {
    expect(countSumOfTwoRepresentations(24,12,12)).toEqual(1);
  });
  test('test6', () => {
    expect(countSumOfTwoRepresentations(93,24,58)).toEqual(12);
  });
  test('test7', () => {
    expect(countSumOfTwoRepresentations(1000000,490000,900000)).toEqual(10001);
  });
  test('test8', () => {
    expect(countSumOfTwoRepresentations(1000,1,1000)).toEqual(500);
  });
  test('test9', () => {
    expect(countSumOfTwoRepresentations(1000000000,999999000,1000000000)).toEqual(0);
  });
  test('test10', () => {
    expect(countSumOfTwoRepresentations(10000,8,9991)).toEqual(4992);
  });
  test('test11', () => {
    expect(countSumOfTwoRepresentations(2400,12,1000000)).toEqual(1189);
  });
  test('test12', () => {
    expect(countSumOfTwoRepresentations(88,27,58)).toEqual(15);
  });
});
