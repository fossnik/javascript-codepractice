export function countSumOfTwoRepresentations(n, l, r) {
	console.log(`l:${l}  r:${r}  n:${n}`);

	if (l + l > n) {
		console.log("Bottom of range is too high");
		return 0;
	}

	if (l + r !== n) {
		console.log("UH-OH!  l + r != n");
		if (l + r < n) {
			console.log(`l is too low`);
			var A = n - r;
			var B = r;
		}

		if (l + r > n) {
			console.log(`r is too high`);
			var A = l;
			var B = n - l;
		}
	}

	else {
		var A = l, B = r;
	}

	console.log(`\nA: ${A}  B: ${B}\n`);

	if (A > B) return 0;

	return Math.floor((B - A) / 2) + 1;
}
