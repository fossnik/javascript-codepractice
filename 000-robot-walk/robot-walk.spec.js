import { robotWalk } from './robot-walk';

describe('Tests', () => {
  test('test1', () => {
    expect(robotWalk([4, 4, 3, 2, 2, 3])).toEqual(true);
  });
  test('test2', () => {
    expect(robotWalk([7, 5, 4, 5, 2, 3])).toEqual(true);
  });
  test('test3', () => {
    expect(robotWalk([10, 3, 10, 2, 5, 1, 2])).toEqual(false);
  });
  test('test4', () => {
    expect(robotWalk([11, 8, 6, 6, 4, 3, 7, 2, 1])).toEqual(true);
  });
  test('test5', () => {
    expect(robotWalk([5, 5, 5, 5])).toEqual(true);
  });
  test('test6', () => {
    expect(robotWalk([34241, 23434, 2341])).toEqual(false);
  });
  test('test7', () => {
    expect(robotWalk([1000, 5, 5, 6, 6, 100])).toEqual(true);
  });
  test('test8', () => {
    expect(robotWalk([1000, 5, 5, 4, 4, 3])).toEqual(false);
  });
  test('test9', () => {
    expect(robotWalk([100000, 100000, 10000, 1000, 1000, 100, 100, 10, 10, 5])).toEqual(false);
  });
  test('test10', () => {
    expect(robotWalk([100000, 100000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000])).toEqual(true);
  });
});
