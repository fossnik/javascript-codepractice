export function robotWalk(a) {
	let visitedSquares = new Set();
	let coordinates = {y: 0, x: 0};
	visitedSquares.add(`y: ${coordinates.y}, x: ${coordinates.x}`);

	function move(deltaX, deltaY, distance) {
		let { y, x } = coordinates;

		for (let i = 0; i < distance; i++) {
			y += deltaY, x += deltaX;
			if (visitedSquares.has(`y: ${y}, x: ${x}`)) return true;

			visitedSquares.add(`y: ${y}, x: ${x}`);
		}

		console.log(`from {${coordinates.y}, ${coordinates.x}} to {${y}, ${x}}`);
		coordinates.y = y, coordinates.x = x;
	}

	let orientation = 0;
	for (const distance of a) {
		switch(orientation) {
			case 0: if (move( 1,  0, distance)) return true; break;
			case 1: if (move( 0,  1, distance)) return true; break;
			case 2: if (move(-1,  0, distance)) return true; break;
			case 3: if (move( 0, -1, distance)) return true; break;
		}
		orientation += 1;
		orientation %= 4;
	}

	return false;
}
