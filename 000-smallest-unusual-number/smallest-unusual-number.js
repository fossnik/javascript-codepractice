export function smallestUnusualNumber(a) {
	let array = [...a].map(s => Number(s));

	const sumOfDigits = n => n.reduce((a, b) => a + b, 0);
	const productOfDigits = n => n.reduce((a, b) => a * b, 1);
	const isUnusual = x => sumOfDigits(x) > productOfDigits(x);

	function incrementArray(n = array.length - 1) {
		if (array[n] === 9) {
			array[n] = 0;
			incrementArray(n - 1);
		}
		else {
			array[n]++;
		}
	}

	while (!isUnusual(array)) incrementArray();

	return Number(array.join("")) - Number(a);
}
