import { smallestUnusualNumber } from './smallest-unusual-number';

describe('Tests', () => {
  test('test1', () => {
    expect(smallestUnusualNumber("42")).toEqual(8);
  });
  test('test2', () => {
    expect(smallestUnusualNumber("1")).toEqual(9);
  });
  test('test3', () => {
    expect(smallestUnusualNumber("10")).toEqual(0);
  });
  test('test4', () => {
    expect(smallestUnusualNumber("11")).toEqual(0);
  });
  test('test5', () => {
    expect(smallestUnusualNumber("17")).toEqual(0);
  });
  test('test6', () => {
    expect(smallestUnusualNumber("23")).toEqual(7);
  });
  test('test7', () => {
    expect(smallestUnusualNumber("1000000000000000000000000000000000000")).toEqual(0);
  });
  test('test8', () => {
    expect(smallestUnusualNumber("2017")).toEqual(0);
  });
  test('test9', () => {
    expect(smallestUnusualNumber("8888888888888888888888888888888")).toEqual(2);
  });
  test('test10', () => {
    expect(smallestUnusualNumber("22")).toEqual(8);
  });
  test('test11', () => {
    expect(smallestUnusualNumber("21")).toEqual(0);
  });
});
