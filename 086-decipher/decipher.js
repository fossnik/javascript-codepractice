export function decipher(cipher) {
	let remit = "";

	for (let i = 0; i < cipher.length; i+=2) {
		let c = String.fromCodePoint(cipher.substring(i, i + 2));
		if (isLowerCase(c)) remit += c;
		else {
			c = String.fromCodePoint(cipher.substring(i, ++i + 2));
			if (isLowerCase(c)) remit += c;
		}
	}

	return remit;
}

function isLowerCase(a) {
	return "abcdefghijklmnopqrstuvwxyz".includes(a);
}
