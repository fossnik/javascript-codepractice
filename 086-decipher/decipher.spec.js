import { decipher } from './decipher';

describe('Tests', () => {
  test('test1', () => {
    expect(decipher("10197115121")).toEqual("easy");
  });
  test('test2', () => {
    expect(decipher("98")).toEqual("b");
  });
  test('test3', () => {
    expect(decipher("122")).toEqual("z");
  });
  test('test4', () => {
    expect(decipher("1229897")).toEqual("zba");
  });
  test('test5', () => {
    expect(decipher("97989910010110210310410510610710810911011111211311411511611711811912012112297")).toEqual("abcdefghijklmnopqrstuvwxyza");
  });
  test('test6', () => {
    expect(decipher("10297115106108102108971061151041029897107106115981001029710711510010298")).toEqual("fasjlflajshfbakjsbdfaksdfb");
  });
  test('test7', () => {
    expect(decipher("11211111911310110810910097107108115111112119113101106107971101021101061021041149710511411497")).toEqual("powqelmdaklsopwqejkanfnjfhrairra");
  });
});
