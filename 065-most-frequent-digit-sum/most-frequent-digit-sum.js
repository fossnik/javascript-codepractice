export function mostFrequentDigitSum(n) {
	const s = i => [...String(i)].reduce((a, b) => Number(a) + Number(b));
	let firstSequence = [n];
	while (n !== 0) firstSequence.push(n -= s(n));

	let tally = {};
	firstSequence.forEach(e => {
		const sx = s(e);
		if (tally[sx]) tally[sx]++;
		else tally[sx] = 1;
	});

	let mostFrequentNum;
	let highFrequency = 0;
	for (const e in tally) {
		if (tally[e] >= highFrequency) {
			mostFrequentNum = e;
			highFrequency = tally[e];
		}
	}

	return Number(mostFrequentNum);
}
