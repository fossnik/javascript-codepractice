import { mostFrequentDigitSum } from './most-frequent-digit-sum';

describe('Tests', () => {
  test('test1', () => {
    expect(mostFrequentDigitSum(88)).toEqual(9);
  });
  test('test2', () => {
    expect(mostFrequentDigitSum(8)).toEqual(8);
  });
  test('test3', () => {
    expect(mostFrequentDigitSum(1)).toEqual(1);
  });
  test('test4', () => {
    expect(mostFrequentDigitSum(17)).toEqual(9);
  });
  test('test5', () => {
    expect(mostFrequentDigitSum(239)).toEqual(9);
  });
  test('test6', () => {
    expect(mostFrequentDigitSum(994)).toEqual(9);
  });
  test('test7', () => {
    expect(mostFrequentDigitSum(99999)).toEqual(18);
  });
});
