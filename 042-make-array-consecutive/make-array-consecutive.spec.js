import { makeArrayConsecutive } from './make-array-consecutive';

describe('Tests', () => {
  test('test1', () => {
    expect(makeArrayConsecutive([6, 2, 3, 8])).toEqual(3);
  });
  test('test2', () => {
    expect(makeArrayConsecutive([0, 3])).toEqual(2);
  });
  test('test3', () => {
    expect(makeArrayConsecutive([5, 4, 6])).toEqual(0);
  });
  test('test4', () => {
    expect(makeArrayConsecutive([6, 3])).toEqual(2);
  });
  test('test5', () => {
    expect(makeArrayConsecutive([1])).toEqual(0);
  });
});
