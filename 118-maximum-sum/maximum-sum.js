export function maximumSum(a, q) {
	let rangeHeatMap = new Array(a.length).fill(0);
	for (let range of q) {
		for (let i = range[0]; i <= range[1]; i++) {
			rangeHeatMap[i]++;
		}
	}

	const levelsOfExposure = new Set(...[new Array(...rangeHeatMap).sort((a, b) => b - a)]);
	let numbersAscending = a.sort((a, b) => a - b);

	let optimalArrangement = [];
	for (const exposureLevel of levelsOfExposure) {
		rangeHeatMap.forEach((v, i) => {
			if (v === exposureLevel) {
				optimalArrangement[i] = numbersAscending.pop();
			}
		});
	}

	return q.reduce((sum, range) => {
		let tally = 0;
		for (let i = range[0]; i <= range[1]; i++) {
			tally += optimalArrangement[i];
		}
		return sum + tally;
	}, 0);
}
