import { maximumSum } from './maximum-sum';

describe('Tests', () => {
  test('test1', () => {
    const a = [9, 7, 2, 4, 4];
    const q = [[1,3], [1,4], [0,2]];
    expect(maximumSum(a, q)).toBe(62);
  });
  test('test2', () => {
    const a = [2, 1, 2];
    const q = [[0,1]];
    expect(maximumSum(a, q)).toBe(4);
  });
  test('test3', () => {
    const a = [5, 3, 2];
    const q = [[0,0], [0,1], [1,2], [0,2]];
    expect(maximumSum(a, q)).toBe(28);
  });
  test('test4', () => {
    const a = [5, 2, 4, 1, 3];
    const q = [[0,4], [1,2], [1,2]];
    expect(maximumSum(a, q)).toBe(33);
  });
  test('test5', () => {
    const a = [4, 2, 1, 6, 5, 7, 2, 4];
    const q = [[1,6], [2,4], [3,6], [0,7], [3,6], [4,4], [5,6], [5,6], [0,1], [3,4]];
    expect(maximumSum(a, q)).toBe(162);
  });
});
