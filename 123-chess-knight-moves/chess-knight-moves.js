export function chessKnightMoves(cell) {
	const x = cell.codePointAt(0) - 96;
	const y = Number(cell[1]);

	return [[1,2],
		[2,1],
		[2,-1],
		[1,-2],
		[-1,-2],
		[-2,-1],
		[-2,1],
		[-1,2],]
		.filter(ia => x + ia[0] > 0 && x + ia[0] < 9 && y + ia[1] > 0 && y + ia[1] < 9).length;
}
