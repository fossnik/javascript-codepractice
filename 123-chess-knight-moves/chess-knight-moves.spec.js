import { chessKnightMoves } from './chess-knight-moves';

describe('Tests', () => {
  test('test1', () => {
    expect(chessKnightMoves("a1")).toEqual(2);
  });
  test('test2', () => {
    expect(chessKnightMoves("c2")).toEqual(6);
  });
  test('test3', () => {
    expect(chessKnightMoves("h8")).toEqual(2);
  });
  test('test4', () => {
    expect(chessKnightMoves("d5")).toEqual(8);
  });
  test('test5', () => {
    expect(chessKnightMoves("a2")).toEqual(3);
  });
  test('test6', () => {
    expect(chessKnightMoves("h7")).toEqual(3);
  });
  test('test7', () => {
    expect(chessKnightMoves("h6")).toEqual(4);
  });
  test('test8', () => {
    expect(chessKnightMoves("b2")).toEqual(4);
  });
  test('test9', () => {
    expect(chessKnightMoves("f4")).toEqual(8);
  });
});
