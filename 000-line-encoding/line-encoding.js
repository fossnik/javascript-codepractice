export function lineEncoding(input) {
	let output = "";

	let count = 1;
	let first = '';
	for (const next of input) {
		if (first === next) count++;
		else {
			output += count > 1 ? count + first : first;
			count = 1;
			first = next;
		}
	}

	return output += count > 1 ? count + first : first;
}
