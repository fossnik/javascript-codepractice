import { lineEncoding } from './line-encoding';

describe('Tests', () => {
  test('test1', () => {
    expect(lineEncoding("aabbbc")).toEqual("2a3bc");
  });
  test('test2', () => {
    expect(lineEncoding("abbcabb")).toEqual("a2bca2b");
  });
  test('test3', () => {
    expect(lineEncoding("abcd")).toEqual("abcd");
  });
  test('test4', () => {
    expect(lineEncoding("zzzz")).toEqual("4z");
  });
  test('test5', () => {
    expect(lineEncoding("wwwwwwwawwwwwww")).toEqual("7wa7w");
  });
  test('test6', () => {
    expect(lineEncoding("ccccccccccccccc")).toEqual("15c");
  });
  test('test7', () => {
    expect(lineEncoding("qwertyuioplkjhg")).toEqual("qwertyuioplkjhg");
  });
  test('test8', () => {
    expect(lineEncoding("ssiiggkooo")).toEqual("2s2i2gk3o");
  });
  test('test9', () => {
    expect(lineEncoding("adfaaa")).toEqual("adf3a");
  });
  test('test10', () => {
    expect(lineEncoding("bbjaadlkjdl")).toEqual("2bj2adlkjdl");
  });
});
