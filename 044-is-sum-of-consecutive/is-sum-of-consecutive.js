export function isSumOfConsecutive(n) {
	let consecutives = [];

	for (let start = Math.floor(n / 2); start > 0; start--) {
		let sequence = [];
		let sum = start;
		sequence.push(start);

		let i = start;
		while (sum < n) {
			sum += ++i;
			sequence.push(i);
			if (sum === n) consecutives.push(sequence);
		}
	}

	console.log(consecutives);
	return consecutives.length;
}
