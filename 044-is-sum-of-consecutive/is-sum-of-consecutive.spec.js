import { isSumOfConsecutive } from './is-sum-of-consecutive';

describe('Tests', () => {
  test('test1', () => {
    expect(isSumOfConsecutive(9)).toEqual(2);
  });
  test('test2', () => {
    expect(isSumOfConsecutive(8)).toEqual(0);
  });
  test('test3', () => {
    expect(isSumOfConsecutive(15)).toEqual(3);
  });
  test('test4', () => {
    expect(isSumOfConsecutive(24)).toEqual(1);
  });
  test('test5', () => {
    expect(isSumOfConsecutive(13)).toEqual(1);
  });
  test('test6', () => {
    expect(isSumOfConsecutive(25)).toEqual(2);
  });
  test('test7', () => {
    expect(isSumOfConsecutive(99)).toEqual(5);
  });
});
