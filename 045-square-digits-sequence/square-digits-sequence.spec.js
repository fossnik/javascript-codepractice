import { squareDigitsSequence } from './square-digits-sequence';

describe('Tests', () => {
  test('test1', () => {
    expect(squareDigitsSequence(16)).toEqual(9);
  });
  test('test2', () => {
    expect(squareDigitsSequence(103)).toEqual(4);
  });
  test('test3', () => {
    expect(squareDigitsSequence(1)).toEqual(2);
  });
  test('test4', () => {
    expect(squareDigitsSequence(13)).toEqual(4);
  });
  test('test5', () => {
    expect(squareDigitsSequence(89)).toEqual(9);
  });
  test('test6', () => {
    expect(squareDigitsSequence(612)).toEqual(16);
  });
});
