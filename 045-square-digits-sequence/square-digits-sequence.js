export function squareDigitsSequence(a0) {
	let previous = [a0];
	const getSumOfSquares = n =>
		[...String(n)].reduce((a, b) => a + b ** 2, 0);

	let number = getSumOfSquares(a0);

	while (! previous.includes(number)) {
		console.log(number)
		previous.push(number);
		number = getSumOfSquares(number);
	}

	console.log(previous);
	return previous.length + 1;
}
