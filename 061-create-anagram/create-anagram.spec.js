import { createAnagram } from './create-anagram';

describe('Tests', () => {
  test('test1', () => {
    const s = "AABAA";
    const t = "BBAAA";
    expect(createAnagram(s, t)).toEqual(1);
  });
  test('test2', () => {
    const s = "OVGHK";
    const t = "RPGUC";
    expect(createAnagram(s, t)).toEqual(4);
  });
  test('test3', () => {
    const s = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB";
    const t = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC";
    expect(createAnagram(s, t)).toEqual(1);
  });
  test('test4', () => {
    const s = "HDFFVR";
    const t = "FEDDEE";
    expect(createAnagram(s, t)).toEqual(4);
  });
  test('test5', () => {
    const s = "AABCDS";
    const t = "BASEAE";
    expect(createAnagram(s, t)).toEqual(2);
  });
  test('test6', () => {
    const s = "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZY";
    const t = "YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYZ";
    expect(createAnagram(s, t)).toEqual(31);
  });
  test('test7', () => {
    const s = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    const t = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    expect(createAnagram(s, t)).toEqual(0);
  });
  test('test8', () => {
    const s = "AAAAAA";
    const t = "AAAAAA";
    expect(createAnagram(s, t)).toEqual(0);
  });
  test('test9', () => {
    const s = "KJDMDLEEKALIJB";
    const t = "AFDJGDCGHMIGHB";
    expect(createAnagram(s, t)).toEqual(7);
  });
  test('test10', () => {
    const s = "BBAAABCBCAABB";
    const t = "BBBCCCBABBACA";
    expect(createAnagram(s, t)).toEqual(2);
  });
});
