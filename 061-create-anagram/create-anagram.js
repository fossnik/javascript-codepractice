export function createAnagram(s, t) {
	function getLetterTally(str) {
		let letterTally = {};
		[...str].forEach(c => {
			if (letterTally[c]) letterTally[c]++;
			else letterTally[c] = 1;
		});
		return letterTally;
	}
	function isAnagram(a, b) {
		let letterTallyA = getLetterTally(a);
		let letterTallyB = getLetterTally(b);
		for (let c in letterTallyA) {
			if (letterTallyA[c] !== letterTallyB[c]) return false;
		}
		for (let c in letterTallyB) {
			if (letterTallyA[c] !== letterTallyB[c]) return false;
		}
		return true;
	}
	function removeIndexFromString(str, idx) {
		return str.substr(0, idx) + str.substr(idx + 1);
	}

	if (isAnagram(s, t)) return 0;

	let s1 = s + "";
	let t1 = t + "";

	[...s].forEach(c => {
		const indexS = s1.indexOf(c);
		const indexT = t1.indexOf(c);

		if (indexS !== -1 && indexT !== -1) {
			s1 = removeIndexFromString(s1, indexS);
			t1 = removeIndexFromString(t1, indexT);
		}
	});

	return s1.length;
}
