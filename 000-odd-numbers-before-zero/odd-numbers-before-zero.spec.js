import { oddNumbersBeforeZero } from './odd-numbers-before-zero';

describe('Tests', () => {
  test('test1', () => {
    expect(oddNumbersBeforeZero([1, 2, 3, 0, 4, 5, 6, 0, 1])).toEqual(2);
  });
  test('test2', () => {
    expect(oddNumbersBeforeZero([5, 1, 1, 3, 3, 0])).toEqual(5);
  });
});
