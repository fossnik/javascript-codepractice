export const oddNumbersBeforeZero = sequence => sequence.filter((e, i) => e % 2 != 0 && i < sequence.indexOf(0)).length;
