import { reverseOnDiagonals } from './reverse-on-diagonals';

describe('Tests', () => {
  test('test1', () => {
    const input = [[1,2,3],
      [4,5,6],
      [7,8,9]];
    const expected = [[9,2,7],
      [4,5,6],
      [3,8,1]];
    expect(reverseOnDiagonals(input)).toEqual(expected);
  });
  test('test2', () => {
    const input = [[239]];
    const expected = [[239]];
    expect(reverseOnDiagonals(input)).toEqual(expected);
  });
  test('test3', () => {
    const input = [[1,10],
      [100,1000]];
    const expected = [[1000,100],
      [10,1]];
    expect(reverseOnDiagonals(input)).toEqual(expected);
  });
  test('test4', () => {
    const input = [[43,455,32,103],
      [102,988,298,981],
      [309,21,53,64],
      [2,22,35,291]];
    const expected = [[291,455,32,2],
      [102,53,21,981],
      [309,298,988,64],
      [103,22,35,43]];
    expect(reverseOnDiagonals(input)).toEqual(expected);
  });
  test('test5', () => {
    const input = [[34,1000,139,248,972,584],
      [98,1,398,128,762,654],
      [33,498,132,543,764,43],
      [329,12,54,764,666,213],
      [928,109,489,71,837,332],
      [93,298,42,53,76,43]];
    const expected = [[43,1000,139,248,972,93],
      [98,837,398,128,109,654],
      [33,498,764,54,764,43],
      [329,12,543,132,666,213],
      [928,762,489,71,1,332],
      [584,298,42,53,76,34]];
    expect(reverseOnDiagonals(input)).toEqual(expected);
  });
  test('test6', () => {
    const input = [[1,1],
      [2,2]];
    const expected = [[2,2],
      [1,1]];
    expect(reverseOnDiagonals(input)).toEqual(expected);
  });
});
