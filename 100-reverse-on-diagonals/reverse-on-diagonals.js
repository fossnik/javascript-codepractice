export function reverseOnDiagonals(matrix) {
	for (let x = 0, temp; x < matrix.length / 2; x++) {
		const inv = matrix.length - x - 1;

		temp = matrix[x][x];
		matrix[x][x] = matrix[inv][inv];
		matrix[inv][inv] = temp;

		temp = matrix[x][inv];
		matrix[x][inv] = matrix[inv][x];
		matrix[inv][x] = temp;
	}

	return matrix;
}
