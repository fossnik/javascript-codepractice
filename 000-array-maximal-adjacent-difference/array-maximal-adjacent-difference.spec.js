import { arrayMaximalAdjacentDifference } from './array-maximal-adjacent-difference';

describe('Tests', () => {
  test('test1', () => {
    expect(arrayMaximalAdjacentDifference([2, 4, 1, 0])).toEqual(3);
  });
  test('test2', () => {
    expect(arrayMaximalAdjacentDifference([1, 1, 1, 1])).toEqual(0);
  });
  test('test3', () => {
    expect(arrayMaximalAdjacentDifference([-1, 4, 10, 3, -2])).toEqual(7);
  });
  test('test4', () => {
    expect(arrayMaximalAdjacentDifference([10, 11, 13])).toEqual(2);
  });
  test('test5', () => {
    expect(arrayMaximalAdjacentDifference([-2, -2, -2, -2, -2])).toEqual(0);
  });
  test('test6', () => {
    expect(arrayMaximalAdjacentDifference([-1, 1, -3, -4])).toEqual(4);
  });
});
