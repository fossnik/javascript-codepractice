export function arrayMaximalAdjacentDifference(inputArray) {
	let deltaMax = 0;

	let last = inputArray[0];
	for (const e of inputArray) {
		const delta = Math.abs(e - last);
		if (delta > deltaMax) deltaMax = delta;
		last = e;
	}

	return deltaMax;
}
