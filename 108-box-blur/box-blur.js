export function boxBlur(image) {
	let newImage = new Array(image.length - 2);

	for (let y = 0; y < newImage.length; y++) {
		newImage[y] = new Array(image[0].length - 2);
		for (let x = 0; x < newImage[0].length; x++) {
				const firstRow = image[y][x] +
					image[y][x + 1] +
					image[y][x + 2];

				const secondRow = image[y + 1][x] +
					image[y + 1][x + 1] +
					image[y + 1][x + 2];

				const thirdRow = image[y + 2][x] +
					image[y + 2][x + 1] +
					image[y + 2][x + 2];

			newImage[y][x] = Math.floor((firstRow + secondRow + thirdRow) / 9);
		}
	}

	return newImage;
}
