import { cyclicString } from './cyclic-string';

describe('Tests', () => {
  test('test1', () => {
    expect(cyclicString("cabca")).toEqual(3);
  });
  test('test2', () => {
    expect(cyclicString("aba")).toEqual(2);
  });
  test('test3', () => {
    expect(cyclicString("ccccccccccc")).toEqual(1);
  });
  test('test4', () => {
    expect(cyclicString("bcaba")).toEqual(5);
  });
  test('test5', () => {
    expect(cyclicString("abacabaabacab")).toEqual(7);
  });
  test('test6', () => {
    expect(cyclicString("aab")).toEqual(3);
  });
  test('test7', () => {
    expect(cyclicString("abaaba")).toEqual(3);
  });
  test('test8', () => {
    expect(cyclicString("zazazaza")).toEqual(2);
  });
  test('test9', () => {
    expect(cyclicString("abbaab")).toEqual(4);
  });
});
