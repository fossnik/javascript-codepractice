export function cyclicString(s) {
	let least = s.length;

	for (let i = 0; i < s.length; i++) {
		testAllSubstrings(i);
	}

	function testAllSubstrings(start) {
		for (let end = 1; end < s.length; end++) {
			const substring = s.substring(start, end);

			if (substring.repeat(100).includes(s) && substring.length < least) {
				least = substring.length;
			}
		}
	}

	return least;
}
