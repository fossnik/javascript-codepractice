import { largestEvenNumber } from './largest-even-number';

describe('Tests', () => {
  test('test1', () => {
    expect(smallestUnusualNumber([4, 3, 6, 8, 2, 4])).toEqual(8);
  });
  test('test2', () => {
    expect(smallestUnusualNumber([10, 2])).toEqual(10);
  });
  test('test3', () => {
    expect(smallestUnusualNumber([7, 2])).toEqual(2);
  });
  test('test4', () => {
    expect(smallestUnusualNumber([1, 4, 2, 4, 10])).toEqual(10);
  });
});
