import { candles } from './candles';

describe('Tests', () => {
  test('test1', () => {
    expect(candles(5, 2)).toEqual(9);
  });
  test('test2', () => {
    expect(candles(1, 2)).toEqual(1);
  });
  test('test3', () => {
    expect(candles(3, 3)).toEqual(4);
  });
  test('test4', () => {
    expect(candles(11, 3)).toEqual(16);
  });
  test('test5', () => {
    expect(candles(15, 5)).toEqual(18);
  });
  test('test6', () => {
    expect(candles(14, 3)).toEqual(20);
  });
  test('test7', () => {
    expect(candles(12, 2)).toEqual(23);
  });
  test('test8', () => {
    expect(candles(6, 4)).toEqual(7);
  });
  test('test9', () => {
    expect(candles(13, 5)).toEqual(16);
  });
  test('test10', () => {
    expect(candles(2, 3)).toEqual(2);
  });
});
