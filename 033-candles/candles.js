export function candles(candlesNumber, makeNew) {
	let candlesBurned = 0;
	let leftovers = 0;

	while (candlesNumber > 0) {
		leftovers += candlesNumber;
		candlesBurned += candlesNumber;
		candlesNumber -= candlesNumber;

		if (leftovers >= makeNew) {
			candlesNumber = Math.floor(leftovers / makeNew);
			leftovers = leftovers % makeNew;
		}
	}

	return candlesBurned;
}
