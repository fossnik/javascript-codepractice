import { longestDigitsPrefix } from './longest-digits-prefix';

describe('Tests', () => {
  test('test1', () => {
    expect(longestDigitsPrefix("123aa1")).toEqual("123");
  });
  test('test2', () => {
    expect(longestDigitsPrefix("0123456789")).toEqual("0123456789");
  });
  test('test3', () => {
    expect(longestDigitsPrefix("  3) always check for whitespaces")).toEqual("");
  });
  test('test4', () => {
    expect(longestDigitsPrefix("12abc34")).toEqual("12");
  });
  test('test5', () => {
    expect(longestDigitsPrefix("the output is 42")).toEqual("");
  });
  test('test6', () => {
    expect(longestDigitsPrefix("aaaaaaa")).toEqual("");
  });
});
