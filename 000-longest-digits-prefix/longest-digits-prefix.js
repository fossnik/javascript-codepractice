export function longestDigitsPrefix(inputString) {
	let remit = "";

	for (const e of inputString) {
		if (e.match(/\d/)) remit += e;
		else return remit;
	}

	return remit;
}

