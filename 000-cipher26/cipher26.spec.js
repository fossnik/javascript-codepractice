import { cipher26 } from './cipher26';

describe('Tests', () => {
  test('test1', () => {
    expect(cipher26("taiaiaertkixquxjnfxxdh")).toEqual("thisisencryptedmessage");
  });
  test('test2', () => {
    expect(cipher26("ibttlprimfymqlpgeftwu")).toEqual("itsasecrettoeverybody");
  });
  test('test3', () => {
    expect(cipher26("ftnexvoky")).toEqual("fourtytwo");
  });
  test('test4', () => {
    expect(cipher26("taevzhzmashvjw")).toEqual("thereisnospoon");
  });
  test('test5', () => {
    expect(cipher26("abdgkpvcktdoanbqgxpicxtqon")).toEqual("abcdefghijklmnopqrstuvwxyz");
  });
  test('test6', () => {
    expect(cipher26("z")).toEqual("z");
  });
});
