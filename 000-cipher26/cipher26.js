export const cipher26 = m => {
	let sum = 0;

	return m[0] + [...m].reduce((a, i) => {
		const previous = (i.charCodeAt(0) - 97 - (sum % 26) + 97) % 26;
		sum += previous;
		return a + String.fromCharCode(previous + 97);
	}, '').substring(1);
};
